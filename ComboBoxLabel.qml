import QtQuick 2.15
import QtQuick.Controls 2.15

Column {
    property string labelText: "default label"
    property variant modelData: ["item1", "item2"]
    property int comboboxIndex: 0
    signal changed(int currentIndex)


    Label {
        text: labelText
    }
    ComboBox{
        width: 200
        currentIndex: comboboxIndex
        model: modelData
        onActivated: {
            changed(currentIndex)
        }
    }
}
