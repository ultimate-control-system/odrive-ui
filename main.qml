import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15

import BackendLib 1.0

Window {
    width: 1600
    height: 480
    visible: true
    title: qsTr("Round Trip Test Sender")

    SenderBackend {
        id: backend
    }

    Column {
        spacing: 10

        Row {
            spacing: 10
            padding: 10


            ComboBox {
                width: 200
                model: [ "Axis 0", "Axis 1", "Axis 2" ]
            }

            TextField {
                text: "State: " + backend.axisState
                enabled: false
                width: 150
            }

            TextField {
                text: "Error: " + backend.axisError
                enabled: false
                width: 150
            }

            TextField {
                text: "Motor: " + backend.motorState
                enabled: false
                width: 150
            }
            TextField {
                text: "Encoder: " + backend.encoderState
                enabled: false
                width: 150
            }
            TextField {
                text: "Ctrl: " + backend.controllerState
                enabled: false
                width: 150
            }


        }
        Row {
            spacing: 10
            padding: 10

            TextField {
                text: backend.connected ? qsTr("Connected") : qsTr("Disconnected")
                enabled: false
            }

            TextField {
                text: qsTr("Publishers: ") + backend.subscriberCount
                enabled: false
            }
        }

        Row {
            spacing: 10
            padding: 10

            ComboBoxLabel {
                labelText: "Axis State:"
                comboboxIndex: backend.axisState
                modelData:[
                    "Undefined",
                    "Idle",
                    "Startup Sequence",
                    "Full Calibration Sequence",
                    "Motor Calibration",
                    "Sensorless Control",
                    "Encoder Index Search",
                    "Encoder Offset Calibration",
                    "Closed Loop Control",
                    "Lockin Spin",
                    "Encoder Dir Find",
                    "Homing"
                ]
                onChanged: currentIndex => backend.setAxisState(currentIndex)
            }

            ComboBoxLabel {
                labelText: "Control Mode:"
//                width: 300
                //comboboxIndex: backend.controlMode
                id: controlMode
                modelData: [
                    "Voltage Control",
                    "Torque Control",
                    "Velocity Control",
                    "Position Control"]

                onChanged: currentIndex => backend.setControlMode(currentIndex)
            }
            ComboBoxLabel {
                labelText: "Input Mode:"
//                width: 300
                //comboboxIndex: backend.inputMode
                id: inputMode
                modelData: [
                    "Inactive",
                    "Passthrough",
                    "Velocity Ramp",
                    "Position Filter",
                    "Mix Channels",
                    "Trapeze Trajactory",
                    "Torque Ramp",
                    "Mirror",
                    "Tuning"]

                onChanged: currentIndex => backend.setInputMode(currentIndex)
            }

            Button {
                text: qsTr("Update Controller")
                onClicked: backend.updateController()
            }
        }
        Row {
            spacing: 10
            padding: 10
            TextFieldLabel {
                labelText: "Position:"
                textField: backend.positionEstimate.toFixed(2)
                suffix: " Rotations"
                enableTextField: false
                boxWidth: 200
            }
            TextFieldLabel {
                labelText: "Velocity:"
                textField: backend.velocityEstimate.toFixed(2)
                suffix: " rps"
                enableTextField: false
                boxWidth: 200
            }

            SpinBoxLabel{
                labelText: "Position setpoint:"
                _from: 0
                _value: 1100
                _to: 100 * 100
                _stepSize: 100
                _editable:true
                onChanged: value => backend.setInputPos(value)
            }
            SpinBoxLabel{
                labelText: "Velocity setpoint:"
                _from: 0
                _value: 1100
                _to: 100 * 100
                _stepSize: 100
                _editable:true
                onChanged: value => backend.setInputVel(value)
            }
            TextFieldLabel {
               labelText: "Current:"
               textField: backend.iqMeasured.toFixed(2)
               suffix: " A"
               enableTextField: false
               boxWidth: 200
           }
            TextFieldLabel {
               labelText: "DC Bus:"
               textField: backend.vbusVoltage.toFixed(2)
               suffix: " V"
               enableTextField: false
               boxWidth: 200
           }

            Button {
                text: qsTr("ClearError")
                onClicked: backend.clearError()
            }
            Button {
                text: qsTr("Refresh")
                onClicked: backend.refresh()
            }
            Button {
                text: qsTr("AntiCogging")
                onClicked: backend.anti()
            }

        }
        Row {
            spacing: 10
            padding: 10
            SpinBoxLabel{
                labelText: "Position gain:"
                _from: 0
                _value: 1100
                _to: 100 * 100
                _stepSize: 100
                _editable:true
                onChanged: value => backend.setPosGain(value)
            }
        }
        Row {
            spacing: 10
            padding: 10

            SpinBoxLabel{
                labelText: "Velocity gain:"
                _from: 0
                _value: 1100
                _to: 100 * 100
                _stepSize: 100
                _editable:true
                onChanged: value => backend.setVelGain(value)
            }
            SpinBoxLabel{
                labelText: "Velocity integral gain:"
                _from: 0
                _value: 1100
                _to: 100 * 100
                _stepSize: 100
                _editable:true
                onChanged: value => backend.setVelIntegralGain(value)
            }

            Button {
                text: qsTr("Update gains")
                onClicked: backend.updatePosVelGain()
            }
        }
    }
}

