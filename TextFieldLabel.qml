import QtQuick 2.15
import QtQuick.Controls 2.15

Column {
    property string labelText: "default label"
    property string textField: "default value"
    property string suffix: ""
    property bool enableTextField: true
    property int boxWidth: 200
    //signal changed(int currentIndex)


    Label {
        text: labelText
    }
    TextField{
        width: boxWidth
        text: textField + suffix
        enabled: enableTextField

    }
}