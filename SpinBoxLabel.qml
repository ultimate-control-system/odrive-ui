import QtQuick 2.15
import QtQuick.Controls 2.15

Column {
    property string labelText: "default label"
    property string textField: "default value"
    property string suffix: ""
    property bool enableTextField: true
    property int boxWidth: 200
    //signal changed(int currentIndex)

    property int _from: 0
    property int _value: 110
    property int _to: 100 * 100
    property int _stepSize: 100
    property bool _editable: true
    signal changed(double value)

    Label {
        text: labelText
    }
    SpinBox {
        id: spin
        from: _from
        value: _value
        to: _to
        stepSize: _stepSize
        editable:_editable
        property int decimals: 2
        property real realValue: value / 100
        onValueModified:changed(realValue)
        validator: DoubleValidator {
            bottom: Math.min(spin.from, spin.to)
            top:  Math.max(spin.from, spin.to)
        }

        textFromValue: function(value, locale) {
            return Number(value / 100).toLocaleString(locale, 'f', spin.decimals)
        }

        valueFromText: function(text, locale) {
            return Number.fromLocaleString(locale, text) * 100
        }
    }
}

