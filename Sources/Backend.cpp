#include "Backend.h"

#include "QDebug"
#include <chrono>
using namespace std::chrono_literals;
Backend::Backend(QObject *parent)
    : QObject{parent}
    , _participant(nullptr)
    , _publisher(nullptr)
    , _receivedTimestamp(0)
{
    _message.timestamp(0);
    _message.message("Not set");

    DomainParticipantQos participantQos;
    participantQos.name("Participant_publisher");
    _participant = DomainParticipantFactory::get_instance()->create_participant(0, participantQos);

    if (_participant != nullptr)
    {
        // Create publisher and subscriber
        _publisher = new DdsPublisher<RoundTripPubSubType>(_participant, "RoundTripSend");
        _subscriber = new DdsSubscriber<RoundTripPubSubType>(_participant, "RoundTripReceive");
    }

    _publisher->bindOnPublicationMatched(this, &Backend::_onPublicationMatched);

    _subscriber->bindOnSubscriptionMatched(this, &Backend::_onSubscriptionMatched);
    _subscriber->bindOnDataAvailable(this, &Backend::_onDataAvailable);


    _canVerter = new OdriveCanVerter("CanTransmit", "CanReceive", _participant);

    _axis[0] = new OdriveAxis(_canVerter, 0);
    _axis[0]->bindOnDataAvailable(this,&Backend::_parseData);

    _axis[1] = new OdriveAxis(_canVerter, 1);
    _axis[1]->bindOnDataAvailable(this,&Backend::_parseData);
}

void Backend::_parseData(OdriveMessageId id) {
    switch (id.commandId)
    {
        case EOdriveCommandId::OdriveHeartbeat:
            //qDebug()<<"index: "<<_axisIndex << " : "<< int(_axis[_axisIndex]->state());
            emit heartbeatReceived();
            break;
        case EOdriveCommandId::GetVbusVoltage:
            emit vbusVoltageReceived();
            break;
        case EOdriveCommandId::GetEncoderEstimates:
            qDebug()<<"got enc";
            emit motorfeedbackReceived();
            break;
        case EOdriveCommandId::GetIq:
            qDebug()<<"got iq";
            emit getIqReceived();
            break;
        default: break;
    }
}

Backend::~Backend()
{
    delete _publisher;
    _publisher = nullptr;
    DomainParticipantFactory::get_instance()->delete_participant(_participant);
    _participant = nullptr;
    delete _canVerter;
    _canVerter = nullptr;
    delete _axis[0];
    _axis[0] = nullptr;
    delete _axis[1];
    _axis[1] = nullptr;
}


int Backend::subscriberCount() const
{
    if (!_publisher)
        return 0;

    return _publisher->subscriberCount();
}

bool Backend::connected() const
{
    if (!_subscriber)
        return false;

    return _subscriber->connected();
}

void Backend::_onPublicationMatched(const PublicationMatchedStatus &info)
{
    emit subscriberCountChanged(_publisher->subscriberCount());
}

void Backend::_onSubscriptionMatched(const SubscriptionMatchedStatus &info)
{
    emit connectedChanged(_subscriber->connected());
}

void Backend::_onDataAvailable(const RoundTrip &message, const SampleInfo &info)
{
    auto time = std::chrono::system_clock::now().time_since_epoch();
    _receivedTimestamp = std::chrono::duration_cast<std::chrono::nanoseconds>(time).count();
}

void Backend::setControlMode(int controlMode) {

    qDebug()<<"SetControlMode" << controlMode;
    _controlMode = EOdriveControlMode(controlMode);
}
void Backend::setInputMode(int inputMode) {
    qDebug()<<"SetInputMode" << inputMode;
    _inputMode = EOdriveInputMode(inputMode);
}
void Backend::setAxisState(int state) {
    qDebug()<<"Set_Axis_State" << OdriveAxisStateTextList[state];
    _axis[_axisIndex]->setRequestState(EOdriveAxisState(state));

}

void Backend::setInputPos(double positionSetpoint) {
    qDebug()<<"Set_Input_Pos" << positionSetpoint;
    OdriveInputPosMessage msg;
    msg.inputPosition = positionSetpoint;
    _axis[_axisIndex]->setInputPos(msg);
}

void Backend::setInputVel(double velocitySetpoint) {
    qDebug()<<"Set_Input_Vel" << velocitySetpoint;
    OdriveInputVelMessage msg;
    msg.inputVelocity = velocitySetpoint;
    _axis[_axisIndex]->setInputVel(msg);
}

void Backend::Set_Input_Torque() {
    qDebug()<<"Set_Input_Torque";
}

void Backend::Set_Limits() {
    qDebug()<<"Set_Limits";
}

void Backend::Set_Traj_Vel_Limit() {
    qDebug()<<"Set_Traj_Vel_Limit";
}

void Backend::Set_Traj_Accel_Limits() {
    qDebug()<<"Set_Traj_Accel_Limits";
}

void Backend::Reboot() {
    qDebug()<<"Reboot";
}

void Backend::Clear_Errors() {
    qDebug()<<"Clear_Errors";
}

void Backend::Get_Iq() {
    qDebug()<<"Get_Iq";

}

void Backend::Get_Vbus_Voltage() {
    qDebug()<<"Get_Vbus_Voltage";
}

void Backend::clearError() {
    qDebug()<<"Clear error";
    _axis[_axisIndex]->clearErrors();
}

void Backend::refresh() {
    qDebug()<<"refresh";
    _axis[_axisIndex]->fetchEncoderEstimates();
    std::this_thread::sleep_for(100ms);
    _axis[_axisIndex]->fetchIq();
    std::this_thread::sleep_for(100ms);
    _axis[_axisIndex]->fetchVbusVoltage();
}

void Backend::anti() {
    qDebug()<<"anti";
    _axis[_axisIndex]->startAntiCogging();
}

void Backend::updateController() {
    qDebug()<<"Update controller";
    if(_controlMode < EOdriveControlMode::EndOfList && _inputMode < EOdriveInputMode::EndOfList)
        _axis[_axisIndex]->setControllerModes(_controlMode,_inputMode);
}

void Backend::setPosGain(double positionGain) {
    qDebug()<<"Set pos gain "<<positionGain;
    OdrivePositionGainMessage msgPos;
    msgPos.positionGain = positionGain;
    _axis[_axisIndex]->setPositionGain(msgPos);
}

void Backend::setVelGain(double velocityGain) {
    _velocityGain = velocityGain;
    qDebug()<<"Set vel gains "<<velocityGain;

}
void Backend::setVelIntegralGain(double velocityIntegratorGain) {
    _velocityIntegratorGain = velocityIntegratorGain;
    qDebug()<<"Set vel integral gains "<<velocityIntegratorGain;
}

void Backend::updatePosVelGain() {
    qDebug()<<"Update gains ";
    OdriveVelGainsMessage msg;
    msg.velocityGain = _velocityGain;
    msg.velocityIntegrationGain = _velocityIntegratorGain;
    _axis[_axisIndex]->setVelocityGains(msg);


}




