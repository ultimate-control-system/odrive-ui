//
// Created by Teodor on 29.10.2022.
//

#ifndef ODRIVEDDS_ODRIVEDDSUTILITIES_H
#define ODRIVEDDS_ODRIVEDDSUTILITIES_H

// Forward declarations
class OdriveCanVerter;

// Enums
enum class EOdriveCommandId : uint32_t {
    // CANOpen NMT Message
    // Master
    // (This message is reserved to avoid bus collisions with CANOpen devices. It is not used by CAN Simple)
    CanOpenNmt [[maybe_unused]] = 0x000,
    // ODrive Heartbeat Message
    // Axis
    // Axis Error         | 0 | Unsigned Int | 32
    // Axis Current State | 4 | Unsigned Int | 8
    // Controller Status  | 7 | Bitfield     | 8
    OdriveHeartbeat = 0x001,
    // ODrive Estop Message
    // Master
    OdriveEstop = 0x002,
    // Get Motor Error
    // Axis
    // Motor Error | 0 | Unsigned Int | 64
    GetMotorError = 0x003,
    // Get Encoder Error
    // Axis
    // Encoder Error | 0 | Unsigned Int | 32
    // (This message is call & response. The Master node sends a message with the RTR bit set,
    // and the axis responds with the same ID and specified payload)
    GetEncoderError = 0x004,
    // Get Sensorless Error
    // Axis
    // Sensorless Error | 0 | Unsigned Int | 32
    // (This message is call & response. The Master node sends a message with the RTR bit set,
    // and the axis responds with the same ID and specified payload)
    GetSensorlessError = 0x005,
    // Set Axis Node ID
    // Master
    // Axis CAN Node ID | 0 | Unsigned Int | 32
    SetAxisNodeId = 0x006,
    // Set Axis Requested State
    // Master
    // Axis Requested State | 0 | Unsigned Int | 32
    SetAxisRequestedState = 0x007,
    // Set Axis Startup Config
    // Master
    // (Not yet implemented)
    SetAxisStartupConfig [[maybe_unused]] = 0x008,
    // Get Encoder Estimates
    // Master
    // Encoder Pos Estimate | 0 | IEEE 754 Float | 32
    // Encoder Vel Estimate | 4 | IEEE 754 Float | 32
    // (This message is call & response. The Master node sends a message with the RTR bit set,
    // and the axis responds with the same ID and specified payload)
    GetEncoderEstimates = 0x009,
    // Get Encoder Count
    // Master
    // Encoder Shadow Count | 0 | Signed Int | 32
    // Encoder Count in CPR | 4 | Signed Int | 32
    // (This message is call & response. The Master node sends a message with the RTR bit set,
    // and the axis responds with the same ID and specified payload)
    GetEncoderCount = 0x00A,
    // Set Controller Modes
    // Master
    // Control Mode | 0 | Signed Int | 32
    // Input Mode   | 4 | Signed Int | 32
    SetControllerModes = 0x00B,
    // Set Input Pos
    // Master
    // Input Pos | 0 | IEEE 754 Float | 32
    // Vel FF    | 4 | Signed Int     | 16 | 0.001
    // Torque FF | 6 | Signed Int     | 16 | 0.001
    SetInputPos = 0x00C,
    // Set Input Vel
    // Master
    // Input Vel | 0 | IEEE 754 Float | 32
    // Torque FF | 4 | IEEE 754 Float | 32
    SetInputVel = 0x00D,
    // Set Input Torque
    // Master
    // Input Torque | 0 | IEEE 754 Float | 32
    SetInputTorque = 0x00E,
    // Set Limits
    // Master
    // Velocity Limit | 0 | IEEE 754 Float | 32
    // Current Limit  | 4 | IEEE 754 Float | 32
    SetLimits = 0x00F,
    // Start Anticogging
    // Master
    StartAnticogging = 0x010,
    // Set Traj Vel Limit
    // Master
    // Traj Vel Limit | 0 | IEEE 754 Float | 32
    SetTrajVelLimit = 0x011,
    // Set Traj Accel Limits
    // Master
    // Traj Accel Limit | 0 | IEEE 754 Float | 32
    // Traj Decel Limit | 4 | IEEE 754 Float | 32
    SetTrajAccelLimits = 0x012,
    // Set Traj Inertia
    // Master
    // Traj Inertia | 0 | IEEE 754 Float | 32
    SetTrajInertia = 0x013,
    // Get IQ
    // Axis
    // Iq Setpoint | 0 | IEEE 754 Float | 32
    // Iq Measured | 4 | IEEE 754 Float | 32
    // (This message is call & response. The Master node sends a message with the RTR bit set,
    // and the axis responds with the same ID and specified payload)
    GetIq = 0x014,
    // Get Sensorless Estimates
    // Master
    // Sensorless Pos Estimate | 0 | IEEE 754 Float | 32
    // Sensorless Vel Estimate | 4 | IEEE 754 Float | 32
    // (This message is call & response. The Master node sends a message with the RTR bit set,
    // and the axis responds with the same ID and specified payload)
    GetSensorlessEstimates = 0x015,
    // Reboot ODrive
    // Master
    // (This message can be sent to either address on a given ODrive board)
    RebootOdrive = 0x016,
    // Get Vbus Voltage
    // Master
    // Vbus Voltage | 0 | IEEE 754 Float | 32
    // (This message can be sent to either address on a given ODrive board)
    GetVbusVoltage = 0x017,
    // Clear Errors
    // Master
    ClearErrors = 0x018,
    // Set Linear Count
    // Master
    // Position | 0 | Signed Int | 32
    SetLinearCount = 0x019,
    // Set Position Gain
    // Master
    // Pos Gain | 0 | IEEE 754 Float | 32
    SetPositionGain = 0x01A,
    // Set Vel Gains
    // Master
    // Vel Gain            | 0 | IEEE 754 Float | 32
    // Vel Integrator Gain | 4 | IEEE 754 Float | 32
    SetVelGains = 0x01B,
    // End of list
    EndOfList,
    // CANOpen Heartbeat Message
    // Slave
    // (This message is reserved to avoid bus collisions with CANOpen devices. It is not used by CAN Simple)
//    CanOpenHeartbeatMessage	=	0x700 //TODO Removed this as it solved the warning
};

enum class EOdriveAxisState : uint32_t {
    Undefined = 0,
    Idle = 1,
    StartupSequence = 2,
    FullCalibrationSequence = 3,
    MotorCalibration = 4,
    SensorlessControl = 5,
    EncoderIndexSearch = 6,
    EncoderOffsetCalibration = 7,
    ClosedLoopControl = 8,
    LockinSpin = 9,
    EncoderDirFind = 10,
    EndOfList
};

enum class EOdriveInputMode : uint32_t {
    Inactive = 0,
    Passthrough,
    VelRamp,
    PosFilter,
    MixChannels,
    TrapTraj,
    TorqueRamp,
    Mirror,
    Tuning,
    EndOfList
};

enum class EOdriveControlMode : uint32_t {
    ControlModeVoltageControl = 0,
    ControlModeTorqueControl,
    ControlModeVelocityControl,
    ControlModePositionControl,
    EndOfList
};
// ODrive.Motor.Error
enum class EMotorError : uint64_t {
    None                         = 0x00000000,
    PhaseResistanceOutOfRange = 0x00000001,
    PhaseInductanceOutOfRange = 0x00000002,
    DrvFault                    = 0x00000008,
    ControlDeadlineMissed      = 0x00000010,
    ModulationMagnitude         = 0x00000080,
    CurrentSenseSaturation     = 0x00000400,
    CurrentLimitViolation      = 0x00001000,
    ModulationIsNan            = 0x00010000,
    MotorThermistorOverTemp   = 0x00020000,
    FetThermistorOverTemp     = 0x00040000,
    TimerUpdateMissed          = 0x00080000,
    CurrentMeasurementUnavailable = 0x00100000,
    ControllerFailed            = 0x00200000,
    IBusOutOfRange           = 0x00400000,
    BrakeResistorDisarmed      = 0x00800000,
    SystemLevel                 = 0x01000000,
    BadTiming                   = 0x02000000,
    UnknownPhaseEstimate       = 0x04000000,
    UnknownPhaseVel            = 0x08000000,
    UnknownTorque               = 0x10000000,
    UnknownCurrentCommand      = 0x20000000,
    UnknownCurrentMeasurement  = 0x40000000,
    UnknownVbusVoltage         = 0x80000000,
    UnknownVoltageCommand      = 0x100000000,
    UnknownGains                = 0x200000000,
    ControllerInitializing      = 0x400000000,
    UnbalancedPhases            = 0x800000000,
    EndOfList
};
// Text lists
constexpr const char *OdriveCommandIdTextList[] =
        {
                "CanOpen NMT",
                "Odrive Heartbeat",
                "Odrive Estop",
                "Get Motor Error",
                "Get Encoder Error",
                "Get Sensorless Error",
                "SetAxis Node Id",
                "Set Axis Requested State",
                "Set Axis Startup Config",
                "Get Encoder Estimates",
                "Get Encoder Count",
                "Set Controller Modes",
                "Set Input Pos",
                "Set Input Vel",
                "Set Input Torque",
                "Set Limits",
                "Start Anticogging",
                "Set Traj Vel Limit",
                "Set Traj Accel Limits",
                "Set Traj Inertia",
                "Get Iq",
                "Get Sensorless Estimates",
                "Reboot Odrive",
                "Get Vbus Voltage",
                "Clear Errors",
                "Set Linear Count",
                "Set Position Gain",
                "Set Vel Gains"
        };

constexpr const char *OdriveAxisStateTextList[] =
        {
                "Undefined",
                "Idle",
                "Startup Sequence",
                "Full Calibration Sequence",
                "Motor Calibration",
                "Sensorless Control",
                "Encoder Index Search",
                "Encoder Offset Calibration",
                "Closed Loop Control",
                "Lockin Spin",
                "Encoder Dir Find"
        };

struct OdriveHearbeatMessage {
public:
    OdriveHearbeatMessage() : error(0), state(0), controllerStatus(0), motorStatus(0), encoderStatus(0) {}

    explicit OdriveHearbeatMessage(const uint8_t *data) : error(0), state(0), controllerStatus(0), motorStatus(0),
                                                          encoderStatus(
                                                                  0) { *this = *reinterpret_cast<const OdriveHearbeatMessage *>(data); }

    [[nodiscard]] EOdriveAxisState stateEnum() const { return EOdriveAxisState(state); }

    uint32_t error{};
    uint8_t state{};
    uint8_t motorStatus{}; // ToDo: Figure out the bitfield of this
    uint8_t encoderStatus{}; // ToDo: Figure out the bitfield of this
    uint8_t controllerStatus{}; // ToDo: Figure out the bitfield of this
};

struct OdriveEncoderCountMessage {
public:
    OdriveEncoderCountMessage() : shadowCount(0), countCpr(0) {}

    explicit OdriveEncoderCountMessage(
            const uint8_t *data) { *this = *reinterpret_cast<const OdriveEncoderCountMessage *>(data); }

    int32_t shadowCount;
    int32_t countCpr;
};

struct OdriveEncoderEstimateMessage {
public:
    OdriveEncoderEstimateMessage() : position(0.0), velocity(0.0) {}

    explicit OdriveEncoderEstimateMessage(
            const uint8_t *data) { *this = *reinterpret_cast<const OdriveEncoderEstimateMessage *>(data); }

    float position;
    float velocity;
};

struct OdriveInputPosMessage {
public:
    OdriveInputPosMessage() : inputPosition(0.0), velocityFeedForward(0), torqueFeedForward(0) {}


    float inputPosition;
    int16_t velocityFeedForward;
    int16_t torqueFeedForward;
};

struct OdriveInputVelMessage {
public:
    OdriveInputVelMessage() : inputVelocity(0.0), torqueFeedForward(0.0) {}

    float inputVelocity;
    float torqueFeedForward;
};

struct OdriveIqMessage {
public:
    OdriveIqMessage() : iqSetpoint(0.0), iqMeasured(0.0) {}

    explicit OdriveIqMessage(const uint8_t *data) { *this = *reinterpret_cast<const OdriveIqMessage *>(data); }

    float iqSetpoint;
    float iqMeasured;
};

struct OdriveControllerMessage {
public:
    OdriveControllerMessage() : controlMode(0), inputMode(0) {}

    uint32_t controlMode;
    uint32_t inputMode;
};

struct OdriveLimitMessage {
public:
    OdriveLimitMessage() : velocityLimit(0.0), currentLimit(0.0) {}

    float velocityLimit;
    float currentLimit;
};

struct OdriveTrajVelLimitMessage {
public:
    OdriveTrajVelLimitMessage() : trajectoryVelocityLimit(0.0) {}

    float trajectoryVelocityLimit;
};

struct OdriveTrajAccelLimitsMessage {
public:
    OdriveTrajAccelLimitsMessage() : accelLimit(0.0), decelLimit(0.0) {}

    float accelLimit;
    float decelLimit;
};

struct OdriveTrajInertiaMessage {
public:
    OdriveTrajInertiaMessage() : trajectoryInertia(0.0) {}

    float trajectoryInertia;
};

struct OdriveLinearCountMessage {
public:
    OdriveLinearCountMessage() : position(0) {}

    int32_t position;
};

struct OdrivePositionGainMessage {
public:
    OdrivePositionGainMessage() : positionGain(0.0) {}

    float positionGain;
};

struct OdriveVelGainsMessage {
public:
    OdriveVelGainsMessage() : velocityGain(0.0), velocityIntegrationGain(0.0) {}

    float velocityGain;
    float velocityIntegrationGain;
};

#endif //ODRIVEDDS_ODRIVEDDSUTILITIES_H
