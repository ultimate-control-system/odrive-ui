#ifndef BACKEND_H
#define BACKEND_H

#include <QObject>
#include <QDateTime>

#include "DdsPublisher.hpp"
#include "DdsSubscriber.hpp"
#include "RoundTrip/RoundTripPubSubTypes.h"
#include "Include/OdriveCanVerter.h"

class Backend : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int subscriberCount READ subscriberCount NOTIFY subscriberCountChanged)
    Q_PROPERTY(bool connected READ connected NOTIFY connectedChanged)

    // Odrive Qprop
    Q_PROPERTY(int axisError READ axisError NOTIFY heartbeatReceived)
    Q_PROPERTY(int axisState READ axisState NOTIFY heartbeatReceived)
    Q_PROPERTY(int motorState READ motorState NOTIFY heartbeatReceived)
    Q_PROPERTY(int encoderState READ encoderState NOTIFY heartbeatReceived)
    Q_PROPERTY(int controllerState READ controllerState NOTIFY heartbeatReceived)

    Q_PROPERTY(double velocityEstimate READ velocityEstimate NOTIFY motorfeedbackReceived)
    Q_PROPERTY(double positionEstimate READ positionEstimate NOTIFY motorfeedbackReceived)

    Q_PROPERTY(double iqSetpoint READ iqSetpoint NOTIFY getIqReceived)
    Q_PROPERTY(double iqMeasured READ iqMeasured NOTIFY getIqReceived)

    Q_PROPERTY(double vbusVoltage READ vbusVoltage NOTIFY vbusVoltageReceived)
//    Q_PROPERTY(double di READ vbusVoltage NOTIFY controllerUpdateReceived)



public:
    explicit Backend(QObject *parent = nullptr);

    ~Backend() override;
    [[nodiscard]] int axisState() const{
        return int(_axis[_axisIndex]->state());
    };
    [[nodiscard]] int motorState() const{
        return _motorState;
    };
    [[nodiscard]] int encoderState() const{
        return _encoderState;
    };
    [[nodiscard]] int controllerState() const{
        return _axis[_axisIndex]->controllerStatus();
    };
    [[nodiscard]] int axisError() const{
        return _axis[_axisIndex]->error();
    };
    [[nodiscard]] double velocityEstimate() const{
        return _axis[_axisIndex]->velocity();
    };
    [[nodiscard]] double positionEstimate() const{
        return _axis[_axisIndex]->position();
    };
    [[nodiscard]] double iqSetpoint() const{
        return _axis[_axisIndex]->iqSetpoint();
    };
    [[nodiscard]] double iqMeasured() const{
        return _axis[_axisIndex]->iqMeasured();
    };
    [[nodiscard]] double vbusVoltage() const {
        return _axis[_axisIndex]->vbusVoltage();
    }

    [[nodiscard]] int subscriberCount() const;

    [[nodiscard]] bool connected() const;


    Q_INVOKABLE
    void setControlMode(int controlMode);
    Q_INVOKABLE
    void setInputMode(int inputMode);
    Q_INVOKABLE
    void setAxisState(int state);
    Q_INVOKABLE
    void setInputPos(double positionSetpoint);
    Q_INVOKABLE
    void setInputVel(double velocitySetpoint);
    Q_INVOKABLE
    void setPosGain(double positionGain);
    Q_INVOKABLE
    void setVelGain(double velocityGain);
    Q_INVOKABLE
    void setVelIntegralGain(double velocityIntegratorGain);
    Q_INVOKABLE
    void updatePosVelGain();
    Q_INVOKABLE
    void Set_Input_Torque();
    Q_INVOKABLE
    void Set_Limits();
    Q_INVOKABLE
    void Set_Traj_Vel_Limit();
    Q_INVOKABLE
    void Set_Traj_Accel_Limits();
    Q_INVOKABLE
    void Reboot();
    Q_INVOKABLE
    void Clear_Errors();
    Q_INVOKABLE
    void Get_Iq();
    Q_INVOKABLE
    void Get_Vbus_Voltage();
    Q_INVOKABLE
    void clearError();
    Q_INVOKABLE
    void refresh();
    Q_INVOKABLE
    void anti();
    Q_INVOKABLE
    void updateController();

signals:
    void subscriberCountChanged(int);
    void connectedChanged(bool);
    void heartbeatReceived(); // Heartbeat containing errors and different states
    void motorfeedbackReceived(); // Motor feedback such as speed, position, torque and supply voltage
    void controllerUpdateReceived(); // Controller config such as speed /pos / torque mode
    void vbusVoltageReceived(); // Controller config such as speed /pos / torque mode
    void getIqReceived(); // Controller config such as speed /pos / torque mode

private:
    DomainParticipant* _participant;
    DdsPublisher<RoundTripPubSubType>* _publisher;
    DdsSubscriber<RoundTripPubSubType>* _subscriber;
    OdriveCanVerter* _canVerter;
    OdriveAxis * _axis[2];
    RoundTrip _message;
    uint64_t _receivedTimestamp;
    int _axisState{};
    int _motorState{};
    int _encoderState{};
    int _controllerState{};
    int _axisError{};
    double _velocityEstimate{};
    double _positionEstimate{};
    double _iqSetpoint{};
    double _iqMeasured{};
    EOdriveInputMode _inputMode;
    EOdriveControlMode _controlMode;
    double _velocityIntegratorGain;
    double _velocityGain;

    void _onPublicationMatched(const PublicationMatchedStatus& info);
    void _onSubscriptionMatched(const SubscriptionMatchedStatus& info);
    void _onDataAvailable(const RoundTrip& message, const SampleInfo& info);
    void _parseData(OdriveMessageId id);
    int _axisIndex = 0;
};

#endif // BACKEND_H
